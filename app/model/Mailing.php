<?php

namespace App\Model;

use Nette\Object;

class Mailing extends Object 
{
	/**
	* @var string
	*/
	protected $email;

	/**
	*	@var $email string
	*/
	public function _construct($email)
	{
		$this->email = $email;
	}

	public function setEmail($email)
	{
		$this->email = $email;
	}

	/**
	* @return $string
	*/
	public function getEmail()
	{
		return $this->email;
	}

	/**
	* @return $string
	*/
	public function createEmail($name, $weight, $height, $bmi, $email)
	{
		$values = array(
						'name' => $name,
						'weight' => $weight,
						'height' => $height,
						'bmi' => $bmi,
						'email' => $email,
						);


		// $email->setHtmlBody($template)
		// 	->send();

		// $this->flashMessage('Sprava bola odoslana');
		// $this->redirect('this');
	}
}