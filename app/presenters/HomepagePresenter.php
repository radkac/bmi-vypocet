<?php

namespace App\Presenters;

use Nette,
	App\Model;

use Nette\Mail\Message;
use Nette\Latte\Engine;
use Nette\Mail\SendmailMailer;

use Nette\Application\UI\Form;

/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{
	/**
	* @var \App\Model\Mailing @inject
	*/
	public $mailing;

	protected function actionDefault()
	{

	}

	protected function createComponentBmiForm()
	{
		$form = new Form();
		$form->addText('name', 'Meno: ')
			->setRequired('Zadajte prosim vase meno.');
		$form->addSelect('sex', 'Pohlavie: ', array('muž', 'žena', 'ufon'))
			->setRequired('Ste si isti?')
			->setPrompt('Vyberte jednu z moznosti');
		$form->addText('weight', 'Hmotnosť: ')
			->setRequired('Zadajte prosim hmotnost')
			->addRule(Form::INTEGER, 'Hmotnost musi byt cislo.');
		$form->addText('height', 'Výška: ')
			->setRequired('Zadajte prosim vysku')
			->addRule(Form::INTEGER, 'Vyska musi byt cislo.')
			->addRule(Form::MIN, 'Vyska musi byt nad 100 cm', 100);
		$form->addText('email', 'Email: ')
			->setAttribute('placeholder', '@')
			->setRequired('Zadajte prosim email')
			->addRule(Form::EMAIL, 'Musi mat spravny tvar!');
		$form->addSubmit('submit', 'Odoslať');
		$form->onSuccess[] = array($this, 'bmiFormSuccess');
		$form->onValidate[] = array($this, 'bmiFormValidate');
		return $form;
	}

	public function bmiFormSuccess(Form $form, $values)
	{
		$bmi = ($values->weight / ($values->height*$values->height)) * 10000;
		$this->flashMessage('Vaše BMI je ' . $bmi . ' !');

		$email = new Message;
		$email->setFrom('Admin <test@admin.cz>' )
			->addTo($values['email']);

		\Tracy\Debugger::barDump(__DIR__ .  '/templates/components/email.latte');

		$template = $this->createTemplate();
		$template->setFile(__DIR__ .  '/templates/components/email.latte');
		$template->title = 'Vase BMI';
		$template->bmi = $bmi;
		$template->values = $values;

		$email->setHtmlBody($template);
		$mailer = new SendmailMailer;
		// $mailer->send($email);

		// $template->render(); // vypisanie toho co je v template



		// $mailer = new Nette\Mail\SmtpMailer(array(
  //       'host' => 'smtp.gmail.com',
  //       'username' => 'franta@gmail.com',
  //       'password' => '*****',
  //       'secure' => 'ssl',
		// ));
		// $mailer->send($mail); // na odoslanie cez gmail

		$this->redirect('Homepage:default');
	}

	public function bmiFormValidate(Form $form, $values)
	{
		if($values->sex = 2)
		{
			$form->addError('Ste divny. Musite byt muz alebo zena.');
		}
	}

}
